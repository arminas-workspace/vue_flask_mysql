from flask import Flask, render_template, jsonify, request
from flask_mysqldb import MySQL
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
import json


app = Flask(__name__)

mysql = MySQL(app)

app.config["MYSQL_USER"] = "arminas"
app.config["MYSQL_PASSWORD"] = "1234"
app.config["MYSQL_HOST"] = "localhost"
app.config["MYSQL_DB"] = "webappdb"

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

# ### swagger specific ###
# SWAGGER_URL = '/swagger'
# API_URL = '/static/swagger.json'
# SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
#     SWAGGER_URL,
#     API_URL,
#     config={
#         'app_name': "flask-mysql"
#     }
# )
# app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
# ### end swagger specific ###
SWAGGER_URL = '/api/docs'  # URL for exposing Swagger UI (without trailing '/')
API_URL = 'http://petstore.swagger.io/v2/swagger.json'  # Our API url (can of course be a local resource)

# Call factory function to create our blueprint
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "Test application"
    },
    # oauth_config={  # OAuth config. See https://github.com/swagger-api/swagger-ui#oauth2-configuration .
    #    'clientId': "your-client-id",
    #    'clientSecret': "your-client-secret-if-required",
    #    'realm': "your-realms",
    #    'appName': "your-app-name",
    #    'scopeSeparator': " ",
    #    'additionalQueryStringParams': {'test': "hello"}
    # }
)

# Register blueprint at URL
# (URL must match the one given to factory function above)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

# Now point your browser to localhost:5000/api/docs/




@app.route("/doctors", methods=['GET', 'POST'])
def all_doctors():
    response_object = {'status': 'success'}
    cur = mysql.connection.cursor()
    if request.method == 'POST':
        post_data = request.get_json()
        cur.execute('''insert into doctor(Name, LastName) values(%s, %s);''', (post_data.get('Name'), post_data.get('LastName')))
        mysql.connection.commit()
    else:
        cur.execute('''SELECT * FROM doctor''')
        row_headers=[x[0] for x in cur.description] #this will extract row headers
        results = cur.fetchall()
        json_data=[]
        for result in results:
            json_data.append(dict(zip(row_headers,result)))
        response_object['doctors'] = json_data
    return jsonify(response_object)

@app.route("/doctors/<doctor_id>", methods=['PUT', 'DELETE'])
def single_doctor(doctor_id):
    cur = mysql.connection.cursor()
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        # remove_doctor(doctor_id)
        cur.execute('''update doctor set Name=%s, LastName=%s where idDoctor=%s;''', (post_data.get('Name'), post_data.get('LastName'), doctor_id))
        mysql.connection.commit()
        response_object['message'] = 'Book updated'
    if request.method == 'DELETE':
        post_data = request.get_json()
        cur.execute('''delete from doctor where idDoctor=%s''', [doctor_id])
        mysql.connection.commit()
        response_object['message'] = 'Book deleted'
    return jsonify(response_object)
    
@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run(ssl_context='adhoc')
    # app.run()